import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./components/home/home.component";
import { TeachersComponent } from "./components/teachers/teachers.component";
import { StudentComponent } from "./components/student/student.component";
import { SchoolComponent } from './components/school/school.component';
import { TeacherComponent } from './components/teacher/teacher.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'school/:id', component: SchoolComponent },
  { path: 'teachers', component: TeachersComponent },
  { path: 'teacher/:id', component: TeacherComponent },
  { path: 'student', component: StudentComponent },
  { path: '', pathMatch: 'full', redirectTo: 'teachers' },
  { path: '**', pathMatch: 'full', redirectTo: 'teachers' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
