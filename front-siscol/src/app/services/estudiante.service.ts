import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AsignaturaModel } from '../models/asignatura.model';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  private urlEndPoint: string = 'http://localhost:8080/';

  constructor(
    private http: HttpClient
  ) { }

  findEstudiantes() {
    return this.http.get(`${this.urlEndPoint}estudiantes`);
  }

  findEstudiantesByAsignatura(asignatura: AsignaturaModel) {
    return this.http.post(`${this.urlEndPoint}estudiantes/asignaturas`, asignatura);
  }
}
