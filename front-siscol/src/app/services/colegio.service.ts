import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ColegioModel } from '../models/colegio.model';
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class ColegioService {

  private urlEndPoint: string = 'http://localhost:8080/';

  constructor(
    private http: HttpClient
  ) { }

  findColegio() {
    return this.http.get(`${ this.urlEndPoint }colegios`);
  }

  findColegioById(id: string){
    return this.http.get(`${ this.urlEndPoint }colegios/${ id }`)
    .pipe(map(
      (resp) => resp as ColegioModel
    ));
  }

  private getUpInfo( respObj: Object ){

  }
  
}
