import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ProfesorModel } from '../models/profesor.model';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {
  private urlEndPoint: string = 'http://localhost:8080/';

  constructor(
    private http: HttpClient
  ) { }

  findProfesores() {
    return this.http.get(`${ this.urlEndPoint }profesores`);
  }

  findProfesor(id: string){
    return this.http.get(`${ this.urlEndPoint }profesores/${ id }`)
    .pipe(map(
      (resp) => resp as ProfesorModel
    ));
  }
}
