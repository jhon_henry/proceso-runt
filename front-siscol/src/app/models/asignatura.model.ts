export class AsignaturaModel {
    id: string;
    nombre: string;
    estudiantes: AsignaturaModel[];

    constructor(){
        this.id = "";
        this.nombre = "";
        this.estudiantes = [];
    }
}