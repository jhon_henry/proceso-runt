import { AsignaturaModel } from "./asignatura.model";

export class EstudianteModel{
    id: string;
    nombre: string;
    asignaturas: AsignaturaModel[];

    constructor(){
        this.id = "";
        this.nombre = "";
        this.asignaturas = [];
    }

}