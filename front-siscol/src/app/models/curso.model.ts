export class CursoModel {
    id: string;
    grado: string;
    salon: string;
    asignaturas: [CursoModel];

    constructor() {
        this.id = "";
        this.grado = "";
        this.salon = "";
        this.asignaturas = [new CursoModel()];
    }
}