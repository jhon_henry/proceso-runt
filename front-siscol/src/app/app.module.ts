import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/share/navbar/navbar.component';
import { TeachersComponent } from './components/teachers/teachers.component';
import { StudentComponent } from './components/student/student.component';

//Http 
import { HttpClientModule } from "@angular/common/http";
import { SchoolComponent } from './components/school/school.component';
import { TeacherComponent } from './components/teacher/teacher.component';
import { CardsComponent } from './components/share/cards/cards.component';
import { FooterComponent } from './components/share/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    TeachersComponent,
    StudentComponent,
    SchoolComponent,
    TeacherComponent,
    CardsComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
