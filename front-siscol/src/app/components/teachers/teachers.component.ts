import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfesorModel } from 'src/app/models/profesor.model';
import { ProfesorService } from 'src/app/services/profesor.service';

@Component({
  selector: 'app-teacher',
  templateUrl: './teachers.component.html',
  styles: [

  ]
})
export class TeachersComponent implements OnInit {

  profesores: ProfesorModel[] = [new ProfesorModel()];
  profesor: ProfesorModel = new ProfesorModel();
  param: boolean = true;

  constructor(
    private profesorService: ProfesorService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.profesorService.findProfesores()
      .subscribe((resp: ProfesorModel[]) => this.profesores = resp);
  }

}
