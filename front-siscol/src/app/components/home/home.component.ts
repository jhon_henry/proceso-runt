import { Component, OnInit } from '@angular/core';
import { ColegioModel } from 'src/app/models/colegio.model';
import { CursoModel } from 'src/app/models/curso.model';
import { ColegioService } from "../../services/colegio.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  colegios: ColegioModel[] = [];
  cursos: CursoModel[] = [];

  constructor(
    private colegioService: ColegioService
  ) { }

  ngOnInit(): void {
    this.colegioService.findColegio()
      .subscribe((resp: ColegioModel[]) => this.colegios = resp);
  }

}
