import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styles: [
  ]
})
export class CardsComponent implements OnInit {

  @Input() items: any[] = [];
  @Input() raiz: string = '';

  constructor() { }

  ngOnInit(): void {

  }

}
