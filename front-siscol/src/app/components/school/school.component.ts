import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ColegioModel } from 'src/app/models/colegio.model';
import { ColegioService } from 'src/app/services/colegio.service';

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styles: [
  ]
})
export class SchoolComponent implements OnInit {

  colegio: ColegioModel = new ColegioModel();

  constructor(
    private colegioService: ColegioService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.colegioService.findColegioById(id)
      .subscribe((resp) => this.colegio = resp);

  }
}
