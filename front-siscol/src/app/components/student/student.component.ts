import { Component, OnInit } from '@angular/core';
import { EstudianteModel } from 'src/app/models/estudiante.model';
import { EstudianteService } from 'src/app/services/estudiante.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styles: [
  ]
})
export class StudentComponent implements OnInit {
  estudiantes: EstudianteModel[] = [];

  constructor(
    private estudianteService: EstudianteService
  ) { }

  ngOnInit(): void {
    this.estudianteService.findEstudiantes().subscribe((resp: EstudianteModel[]) =>{
      console.log(resp);
      this.estudiantes = resp;
    });
  }

}
