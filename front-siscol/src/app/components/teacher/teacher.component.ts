import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AsignaturaModel } from 'src/app/models/asignatura.model';
import { ProfesorModel } from 'src/app/models/profesor.model';
import { EstudianteService } from 'src/app/services/estudiante.service';
import { ProfesorService } from 'src/app/services/profesor.service';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styles: [
    'a.nav-link { color: white; }'
  ]
})
export class TeacherComponent implements OnInit {

  profesor: ProfesorModel = new ProfesorModel();
  estudiantes: any;

  constructor(
    private profesorService: ProfesorService,
    private estudianteService: EstudianteService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.profesorService.findProfesor(id)
      .subscribe((resp) => {
        this.profesor = resp;

        for (var i = 0; i < this.profesor.asignaturas.length; i++) {

          let asignatura: AsignaturaModel = new AsignaturaModel();
          asignatura = this.profesor.asignaturas[i];

          this.estudianteService.findEstudiantesByAsignatura(asignatura)
            .subscribe((resp: []) => {
              asignatura.estudiantes = resp;
              if (asignatura === this.profesor.asignaturas[i]) {
                this.profesor.asignaturas[i] = asignatura;
              }
            });
        }
      });

  }

}
