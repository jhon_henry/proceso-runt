package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Profesor;

public interface IProfesorService {
	
	public List<Profesor> findAll();
	
	public Profesor findOne(Long id);

}
