package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Asignatura;

public interface IAsignaturaService {
	
	public List<Asignatura> findAll();
	
	public Asignatura findOne(Long id);
	
}
