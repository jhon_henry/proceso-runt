package com.jhonmurillo.springboot.backend.siscol.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Profesor;
import com.jhonmurillo.springboot.backend.siscol.models.services.IEstudianteService;
import com.jhonmurillo.springboot.backend.siscol.models.services.IProfesorService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/profesores")
public class ProfesorRestController {

	@Autowired
	private IProfesorService profesorService;
	@Autowired
	IEstudianteService estudianteService;

	@GetMapping
	public List<Profesor> index() {
		return profesorService.findAll();
	}

	@GetMapping("/{id}")
	public Profesor buscarProfesor(@PathVariable Long id) {
		return profesorService.findOne(id);
	}

}
