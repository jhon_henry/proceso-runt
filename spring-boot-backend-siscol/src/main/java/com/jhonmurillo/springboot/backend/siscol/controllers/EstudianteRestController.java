package com.jhonmurillo.springboot.backend.siscol.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Asignatura;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Estudiante;
import com.jhonmurillo.springboot.backend.siscol.models.services.IEstudianteService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/estudiantes")
public class EstudianteRestController {
	
	@Autowired
	private IEstudianteService estudianteService;
	
	@GetMapping
	public List<Estudiante> index(){
		return estudianteService.findAll();
	}
	
	@PostMapping(path = "/asignaturas", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<Estudiante> getByAsignatura(@RequestBody Asignatura info){
		return estudianteService.findAllByAsignaturaId(info);
		
	}

}
