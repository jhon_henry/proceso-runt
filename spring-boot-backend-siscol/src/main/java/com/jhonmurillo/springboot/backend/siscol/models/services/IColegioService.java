package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Colegio;

public interface IColegioService {
	
	public List<Colegio> findAll();
	
	public Colegio findOne(Long Id);

}
