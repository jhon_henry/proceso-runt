package com.jhonmurillo.springboot.backend.siscol.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Colegio;

public interface IColegioDao extends JpaRepository<Colegio, Long>{

}
