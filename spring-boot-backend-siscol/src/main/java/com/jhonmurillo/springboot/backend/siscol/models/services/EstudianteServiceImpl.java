package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jhonmurillo.springboot.backend.siscol.models.dao.IEstudianteDao;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Asignatura;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Estudiante;

@Service
public class EstudianteServiceImpl implements IEstudianteService{

	@Autowired
	private IEstudianteDao estudianteDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Estudiante> findAll() {
		return (List<Estudiante>) estudianteDao.findAll();
	}

	@Override
	public List<Estudiante> findAllByAsignaturaId(Asignatura asignaturaId) {
		return estudianteDao.findByAsignaturas(asignaturaId);
	}

}