package com.jhonmurillo.springboot.backend.siscol.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Profesor;

public interface IProfesorDao extends JpaRepository<Profesor, Long>{

}
