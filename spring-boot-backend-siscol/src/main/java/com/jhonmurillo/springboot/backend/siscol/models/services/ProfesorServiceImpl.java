package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jhonmurillo.springboot.backend.siscol.models.dao.IProfesorDao;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Profesor;

@Service
public class ProfesorServiceImpl implements IProfesorService{

	@Autowired
	private IProfesorDao profesorDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Profesor> findAll() {
		return (List<Profesor>) profesorDao.findAll();
	}

	@Override
	public Profesor findOne(Long id) {
		
		return profesorDao.findById(id).orElse(null);
	}

}
