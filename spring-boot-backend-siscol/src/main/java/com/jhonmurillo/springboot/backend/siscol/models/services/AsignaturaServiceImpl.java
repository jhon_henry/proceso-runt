package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jhonmurillo.springboot.backend.siscol.models.dao.IAsignaturaDao;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Asignatura;

@Service
public class AsignaturaServiceImpl implements IAsignaturaService {

	@Autowired
	private IAsignaturaDao asignaturaDao;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public List<Asignatura> findAll() {
		return (List<Asignatura>) asignaturaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Asignatura findOne(Long id) {
		return asignaturaDao.findById(id).orElse(null);
	}
	
}
