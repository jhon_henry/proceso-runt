package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Asignatura;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Estudiante;

public interface IEstudianteService {
	
	public List<Estudiante> findAll();
	
	public List<Estudiante> findAllByAsignaturaId(Asignatura asignaturaId);

}
