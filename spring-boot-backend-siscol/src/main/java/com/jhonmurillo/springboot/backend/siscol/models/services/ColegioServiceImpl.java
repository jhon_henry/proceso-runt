package com.jhonmurillo.springboot.backend.siscol.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jhonmurillo.springboot.backend.siscol.models.dao.IColegioDao;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Colegio;

@Service
public class ColegioServiceImpl implements IColegioService {

	@Autowired
	private IColegioDao colegioDao;

	@Override
	@Transactional(readOnly = true)
	public List<Colegio> findAll() {
		return (List<Colegio>) colegioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Colegio findOne(Long id) {
		return colegioDao.findById(id).orElse(null);
	}

}
