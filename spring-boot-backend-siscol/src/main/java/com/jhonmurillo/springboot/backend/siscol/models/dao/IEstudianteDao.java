package com.jhonmurillo.springboot.backend.siscol.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Asignatura;
import com.jhonmurillo.springboot.backend.siscol.models.entity.Estudiante;

public interface IEstudianteDao extends JpaRepository<Estudiante, Long>{
	
	public List<Estudiante> findByAsignaturas(Asignatura asignatura);

}
