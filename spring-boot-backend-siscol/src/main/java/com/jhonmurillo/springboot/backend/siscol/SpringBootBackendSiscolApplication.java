package com.jhonmurillo.springboot.backend.siscol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBackendSiscolApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBackendSiscolApplication.class, args);
	}

}
