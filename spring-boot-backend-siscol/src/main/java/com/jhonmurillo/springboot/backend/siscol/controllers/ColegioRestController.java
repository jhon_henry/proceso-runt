package com.jhonmurillo.springboot.backend.siscol.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jhonmurillo.springboot.backend.siscol.models.entity.Colegio;
import com.jhonmurillo.springboot.backend.siscol.models.services.IColegioService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/colegios")
public class ColegioRestController {
	
	@Autowired
	private IColegioService colegioService;
	
	@GetMapping
	public List<Colegio> index(){
		return colegioService.findAll();
	}
	
	@GetMapping("/{id}")
	public Colegio buscarColegio(@PathVariable Long id) {
		return colegioService.findOne(id);
	}

}
